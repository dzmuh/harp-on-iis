var connect = require('connect');
var harp = require('harp');
var http = require('http');
var port = process.env.port || 1337;

var app = connect()
    //.use(connect.basicAuth('username', 'password'))
    .use(connect.favicon('public/favicon.ico'))
    .use(harp.mount(__dirname + '/public'))
    ;

http.createServer(app).listen(port);